// WARNING: This file is generated automatically, do not edit!
// Its content is created from the [defines] section in project.cfg of the
// current project and the build.cfg in each include directory. 
// ----------------------------------------------------------------------------

#ifndef XPCC_CONFIG_HPP
#define	XPCC_CONFIG_HPP

#define CPU_BOARD2_MASTER 0
#define CPU_BOARD2_SLAVE 0
#define DRIVER_CONNECTIVITY_I2C_DEBUG 0
#define INTERCONNECT_PIN_INPUT -1
#define INTERCONNECT_PIN_OUTPUT -1
#define INTERCONNECT_USART_ID -1
#define INTERCONNECT_USART_PORT -1
#define MCP2515_CLOCK 16000000
#define S65_LPC_ACCELERATED 0
#define SPIC0_RX_DMA_CHANNEL -1
#define SPIC0_TX_DMA_CHANNEL -1
#define SPIC1_RX_DMA_CHANNEL -1
#define SPIC1_TX_DMA_CHANNEL -1
#define SPID0_RX_DMA_CHANNEL -1
#define SPID0_TX_DMA_CHANNEL -1
#define SPID1_RX_DMA_CHANNEL -1
#define SPID1_TX_DMA_CHANNEL -1
#define SPIE0_RX_DMA_CHANNEL -1
#define SPIE0_TX_DMA_CHANNEL -1
#define SPIE1_RX_DMA_CHANNEL -1
#define SPIE1_TX_DMA_CHANNEL -1
#define SPIF0_RX_DMA_CHANNEL -1
#define SPIF0_TX_DMA_CHANNEL -1
#define SPIF1_RX_DMA_CHANNEL -1
#define SPIF1_TX_DMA_CHANNEL -1
#define ST7036_VOLTAGE 3
#define UARTC0_CTS_PIN -1
#define UARTC0_CTS_PORT -1
#define UARTC0_RTS_PIN -1
#define UARTC0_RTS_PORT -1
#define UARTC0_RX_BUFFER_SIZE 8
#define UARTC0_TX_BUFFER_SIZE 16
#define UARTC1_CTS_PIN -1
#define UARTC1_CTS_PORT -1
#define UARTC1_RTS_PIN -1
#define UARTC1_RTS_PORT -1
#define UARTC1_RX_BUFFER_SIZE 8
#define UARTC1_TX_BUFFER_SIZE 16
#define UARTD0_CTS_PIN -1
#define UARTD0_CTS_PORT -1
#define UARTD0_RTS_PIN -1
#define UARTD0_RTS_PORT -1
#define UARTD0_RX_BUFFER_SIZE 8
#define UARTD0_TX_BUFFER_SIZE 16
#define UARTD1_CTS_PIN -1
#define UARTD1_CTS_PORT -1
#define UARTD1_RTS_PIN -1
#define UARTD1_RTS_PORT -1
#define UARTD1_RX_BUFFER_SIZE 8
#define UARTD1_TX_BUFFER_SIZE 16
#define UARTE0_CTS_PIN -1
#define UARTE0_CTS_PORT -1
#define UARTE0_RTS_PIN -1
#define UARTE0_RTS_PORT -1
#define UARTE0_RX_BUFFER_SIZE 8
#define UARTE0_TX_BUFFER_SIZE 16
#define UARTE1_CTS_PIN -1
#define UARTE1_CTS_PORT -1
#define UARTE1_RTS_PIN -1
#define UARTE1_RTS_PORT -1
#define UARTE1_RX_BUFFER_SIZE 8
#define UARTE1_TX_BUFFER_SIZE 16
#define UARTF0_CTS_PIN -1
#define UARTF0_CTS_PORT -1
#define UARTF0_RTS_PIN -1
#define UARTF0_RTS_PORT -1
#define UARTF0_RX_BUFFER_SIZE 8
#define UARTF0_TX_BUFFER_SIZE 16
#define UARTF1_CTS_PIN -1
#define UARTF1_CTS_PORT -1
#define UARTF1_RTS_PIN -1
#define UARTF1_RTS_PORT -1
#define UARTF1_RX_BUFFER_SIZE 8
#define UARTF1_TX_BUFFER_SIZE 16
#define XPCC__CLOCK_TESTMODE 1

#endif	// XPCC_CONFIG_HPP

