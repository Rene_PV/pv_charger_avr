

#include <xpcc/architecture.hpp>

#include <xpcc/driver/connectivity/can/mcp2515.hpp>
#include <xpcc/driver/connectivity/spi/software_spi.hpp>

//#include "clksys_driver.c"

//void system_clocks_init(void);

void SpiInit(void);
void Clock_extern(void);
void send_data (int ident, int Anzahl, int data0,  int data1, int data2, int data3, int data4, int data5, int data6, int data7 );


GPIO__OUTPUT(Cs, D, 4);
GPIO__INPUT(Int, D, 3);

GPIO__OUTPUT(Sclk, D, 7);
GPIO__OUTPUT(Mosi, D, 5);
GPIO__INPUT(Miso, D, 6);

typedef xpcc::SoftwareSpi<Sclk, Mosi, Miso> Spi;

xpcc::Mcp2515<Spi, Cs, Int> mcp2515;

// Default filters to receive any extended CAN frame


FLASH_STORAGE(uint8_t canFilter[]) =
{
	MCP2515_FILTER_EXTENDED(0),	// Filter 0
	MCP2515_FILTER_EXTENDED(0),	// Filter 1

	MCP2515_FILTER_EXTENDED(0),	// Filter 2
	MCP2515_FILTER_EXTENDED(0),	// Filter 3
	MCP2515_FILTER_EXTENDED(0),	// Filter 4
	MCP2515_FILTER_EXTENDED(0),	// Filter 5

	MCP2515_FILTER_EXTENDED(0x00),	// Mask 0
	MCP2515_FILTER_EXTENDED(0x00),	// Mask 1
};


int
main()
{

PORTA.DIR |= (1<<PIN4); // LED Port Richtung setzten 
PORTA.OUTTGL  |= (1<<PIN4); // LED Port Anschalten 

// 

PORTB.DIR |= (1<<PIN1); // LED Port Richtung setzten 
//PORTB.OUTTGL |= (1<<PIN1); // LED Port Anschalten 

	/* system clocks initialization */
	Clock_extern(); 

	// Initialize SPI interface and the other pins
	// needed by the MCP2515
	Spi::initialize();
	Cs::setOutput();
	Int::setInput(xpcc::atxmega::PULLUP);
	
	// Configure MCP2515 and set the filters
	mcp2515.initialize(xpcc::can::BITRATE_50_KBPS);
	//mcp2515.setFilter(xpcc::accessor::asFlash(canFilter));
	
	// Create a new message
	
	/*xpcc::can::Message message(0x23);
	message.length = 2;
	message.data[0] = 0x10;
	message.data[1] = 0xaa;
	
	mcp2515.sendMessage(message);
	
	*/
		
	while (1)
	{
		
send_data(0x12, 8, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08);
_delay_ms(5000);
send_data(0x13, 8, 0x09, 0x010, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16);
_delay_ms(5000);
send_data(0x14, 8, 0x17, 0x018, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24);
_delay_ms(5000);



	}
}



// Clock intern verwenden 
void Clock_init(void)
{
   OSC.CTRL |= OSC_RC32MEN_bm;
   while(!(OSC.STATUS & OSC_RC32MRDY_bm));
   CCP = CCP_IOREG_gc;
   CLK.CTRL = CLK_SCLKSEL_RC32M_gc;
}



// Externe Clock Verwenden 
void Clock_extern(void)
{
   OSC_XOSCCTRL = OSC_XOSCSEL_XTAL_16KCLK_gc |
   OSC_FRQRANGE_12TO16_gc;
   OSC.CTRL |= OSC_XOSCEN_bm;
   while(!(OSC.STATUS & OSC_XOSCRDY_bm));
   CCP = CCP_IOREG_gc;
   CLK.CTRL = CLK_SCLKSEL_XOSC_gc;
}



// SPI des DAC 
void SpiInit(void)
{
  PORTC.DIRCLR = PIN6_bm; // miso auf input
  PORTC.DIRSET = PIN7_bm; // clock auf output
  PORTC.DIRSET = PIN5_bm; // mosi auf output
  PORTC.PIN4CTRL = PORT_OPC_WIREDANDPULL_gc; 	// Pull-up SS 
  // enable SPI master mode, CLK/64 (@32MHz=>500KHz)
  SPIC.CTRL = SPI_ENABLE_bm | SPI_MASTER_bm | SPI_MODE_0_gc | SPI_PRESCALER_DIV4_gc;
  // enable LOW LEVEL INTERRUPT
  SPIC.INTCTRL = SPI_INTLVL_LO_gc;
};

void send_data (int ident, int Anzahl, int data0,  int data1, int data2, int data3, int data4, int data5, int data6, int data7 ) 
{
xpcc::can::Message message(ident);
		message.length = Anzahl;
			
			message.data[0] = data0;
			message.data[1] = data1;
			message.data[2] = data2;
			message.data[3] = data3;
			message.data[4] = data4;
			message.data[5] = data5;
			message.data[6] = data6;
			message.data[7] = data7;
	
			mcp2515.sendMessage(message);
			//_delay_ms(5000);

}
